=============
Agenda en PHP
=============

:autor: Jose Francisco Artigas Alc�zar
:version: 1.0

.. header:

*La agenda en PHP es una aplicaci�n web que le permite almacenar algunos datos de sus contactos, as� como borrarlos, editarlos o modificarlos.* 

*Est� escrita en PHP y utiliza una base de datos MySQL.*

*Asimismo utiliza un motor de plantillas llamado Twig*

Si quiere puede ejecutarla en http://agendaenphp.josefco.infenlaces.com

Logéese con root, alumno

.. body:

Instalaci�n
------------
* En Eclipse: file, import, Git, Clone URI, https://JFAABitDevelope@bitbucket.org/JFAABitDevelope/agendanueva.git
* Indique usuario y contrase�a.
* Se le pedir� una ubicaci�n para el proyecto

Twig
----
Twig es un motor de plantillas html. Necesitar� descargarlo. Si tiene **curl** instalado en su sistema, puede saltarse los **dos** siguientes pasos.

* sudo apt-get install curl **para ubuntu** 
* http://curl.haxx.se/download.html **para windows**. Elija la version con soporte ssl
* Abra un terminal. Instale Twig via *composer*:
* Navegue hasta la ra�z del proyecto
* Escriba: **curl -s http://getcomposer.org/installer | php** (Es necesario a�adir php a la variable de entorno path en windows si no la ha a�adido ya)
* Cuando termine el proceso del paso anterior, esciba **php composer.phar install**
* En la raiz del proyecto, cree un archivo llamado **.htaccess** y a�ada:
   * <files config.php>

        order allow,deny

        deny from all

     </files>

Base de datos
-------------
Necesitar� crear una base de datos MySQL en su servidor local, o bien en un servidor remoto si piensa subir la aplicaci�n a un host que provea soporte a MySQL

La herramienta recomendada es phpmyadmin: http://www.phpmyadmin.net/home_page/index.php

Los datos de la base de datos son:

* Nombre de la BD: AgendaPHP
* Nombre de la tabla: contactos
* Campos: 
   * id: INT; longitud --> 4; PRIMARY AI
   * Nombre: VARCHAR; longitud --> 40
   * Apellido: VARCHAR; longitud --> 40
   * Telefono: VARCHAR; longitud --> 9; UNIQUE
   * Correo: VARCHAR; longitud --> 100; UNIQUE




