<?php
	//Archivo que recoge los datos de configuración de nuestra aplicación
	
	//Datos de administrador
	$root = 'root';
	$passwd_admin = 'alumno';
	
	//Configuración de TWIG
	//Para facilitar la portabilidad, suele ponerse como sigue a continuación, Realpath lo que hace es definir
	//la ruta de un archivo. El __FILE__ carga el archivo que se está ejecutando y la linea a continuación añade
	//la ruta, creando así una ruta absoluta
	function config_twig(){
		require_once realpath(dirname(__FILE__)."/../vendor/twig/twig/lib/Twig/Autoloader.php");
		
		//Para cargar las plantillas desde el cargador de plantillas de TWIG tenemos que decirle al sistema donde
		//están las plantillas
		Twig_Autoloader::register();
		$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__).
											"/../vistas"));
		
		$twig = new Twig_Environment($loader);
		$escaper = new Twig_Extension_Escaper(true);
		$twig->addExtension($escaper);
		$twig->getExtension('core')->setTimezone('Europe/Madrid');
		return $twig;								
	}
	
	//Funcion para generar una clave cifrada a partir de una cadena indicada arriba, en este archivo
	function generateHash($passwd_admin) {
		if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
			$salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
			return crypt($passwd_admin, $salt);
		}
	}
	
	//Almacenamos la clave cifrada
	$admin_pass = generateHash($passwd_admin)
?>